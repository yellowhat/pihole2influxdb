## [1.2.11](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.10...v1.2.11) (2023-02-24)


### Bug Fixes

* **deps:** update dependency influxdb-client to v1.36.1 ([5d6a986](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/5d6a986945bdadcafe511a971dcd18114619cb0b))

## [1.2.10](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.9...v1.2.10) (2023-02-11)


### Bug Fixes

* **deps:** update python:3.11.2-alpine3.17 docker digest to 1c7b5a9 ([d634f88](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/d634f889cf47ccaad1e672a465e67a94eae45291))

## [1.2.9](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.8...v1.2.9) (2023-02-09)


### Bug Fixes

* python 3.11.2 ([8ed99ad](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/8ed99ad540e1b5bab9e6ad507a47f86095c458cc))

## [1.2.8](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.7...v1.2.8) (2023-02-05)


### Bug Fixes

* **deps:** update python:3.11.1-alpine3.17 docker digest to 9f95567 ([533afdc](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/533afdca784586c7992a6d96999ff6986ae23588))
* **deps:** update python:3.11.1-alpine3.17 docker digest to 9f95567 ([208eca8](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/208eca899b45a7b95d1880e943766090ada0fb98))

## [1.2.7](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.6...v1.2.7) (2023-01-31)


### Bug Fixes

* use utils template ([7dec02a](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/7dec02a8d929402aef7b37da697ee54933bfd511))

## [1.2.6](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.5...v1.2.6) (2023-01-27)


### Bug Fixes

* **deps:** update dependency influxdb-client to v1.36.0 ([6cebb3f](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/6cebb3f09aee9f571416c15a28b20cd509436725))

## [1.2.5](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.4...v1.2.5) (2023-01-24)


### Bug Fixes

* update python:3.11.1-alpine3.17 docker digest to 8deb764 ([1b64ece](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/1b64ecef121a21a1ceeb3bfb968d70a4330db9d8))
* update python:3.11.1-alpine3.17 docker digest to 8deb764 ([5d8edae](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/5d8edaeb2895a205d7231e44f09538c47a87355e))

## [1.2.4](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.3...v1.2.4) (2023-01-18)


### Bug Fixes

* use alpine 3.17 python image ([c9a59e2](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/c9a59e2822bd9f798cf5080d2f0d5e26a49c2b99))
* use alpine 3.17 python image ([07cc98d](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/07cc98da3630e9971ea473d1c564a974d587f843))

## [1.2.3](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.2...v1.2.3) (2023-01-13)


### Bug Fixes

* **deps:** update dependency requests to v2.28.2 ([87512ad](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/87512add581cc78b701a7baf0f8b523d89aa73df))

## [1.2.2](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.1...v1.2.2) (2023-01-08)


### Bug Fixes

* update python:3.11.1-alpine3.16 docker digest to feaf79d ([44d3ba3](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/44d3ba3113e935bddc8b9fa9ff04b2ac67afd010))
* update python:3.11.1-alpine3.16 docker digest to feaf79d ([a7162ed](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/a7162ed4076fccae33bf940289088590fc98b635))

## [1.2.1](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.2.0...v1.2.1) (2023-01-04)


### Bug Fixes

* compatible with pihole 2022.12.1 ([69e46c6](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/69e46c61f3db3fca60a1eb97a3b916dad517b898))

# [1.2.0](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.1.4...v1.2.0) (2022-12-14)


### Features

* store summary data ([5f175a7](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/5f175a7028fd949d63fd3549b42d086e2bd3bc58))

## [1.1.4](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.1.3...v1.1.4) (2022-12-09)

## [1.1.3](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.1.2...v1.1.3) (2022-12-08)


### Bug Fixes

* update python to v3.11.1 ([28de6f4](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/28de6f4273671d2c80c2061d87a7d1c9946b7365))
* update python to v3.11.1 ([0f1fc86](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/0f1fc8680832164a2fb07f7977f082c37a524040))

## [1.1.2](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.1.1...v1.1.2) (2022-12-07)


### Bug Fixes

* set version inside script ([2c5b16f](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/2c5b16ffc5939ce957c43eb8f1d4ed5c4473e70c))

## [1.1.1](https://gitlab.com/yellowhat-labs/pihole2influxdb/compare/v1.1.0...v1.1.1) (2022-12-04)


### Bug Fixes

* generation of CHANGELOG.md ([78382ee](https://gitlab.com/yellowhat-labs/pihole2influxdb/commit/78382eefcfeaa5b2a4f6c2051428b673476b7303))
