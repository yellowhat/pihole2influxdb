#!/usr/bin/env bash
set -euo pipefail

VERSION=$1

echo "[INFO] Current:"
grep "__version__" pihole2influxdb.py

sed -e "s|__version__ =.*|__version__ = \"$VERSION\"|" \
    -i pihole2influxdb.py

echo "[INFO] Replaced:"
grep "__version__" pihole2influxdb.py
