#!/usr/bin/env bash
set -euo pipefail

POD=influxdb
INFLUXDB_URL="http://127.0.0.1:8086"
INFLUXDB_TOKEN="0123456789ABCDEF"
INFLUXDB_ORG="myorg"
INFLUXDB_BUCKET="pihole"
PIHOLE_URL="http://127.0.0.1:8080"
PIHOLE_PASSWORD="pihole" # pragma: allowlist secret
PIHOLE_INTERVAL=10

podman pod create \
    --name "$POD" \
    --publish 3000:3000 \
    --publish 8080:80 \
    --publish 8086:8086

# InfluxDB
rm -r "${HOME}/.influxdbv2" || true

podman run \
    --name "${POD}_influxdb" \
    --pod "$POD" \
    --detach \
    docker.io/influxdb:latest --reporting-disabled

curl -L https://dl.influxdata.com/influxdb/releases/influxdb2-client-2.5.0-linux-amd64.tar.gz |
    tar xz --strip-components 1

./influx setup \
    --username "username" \
    --password "password" \
    --token "$INFLUXDB_TOKEN" \
    --org "$INFLUXDB_ORG" \
    --bucket default \
    --retention 0 \
    --force

./influx user list

# PiHole
podman run \
    --name "${POD}_pihole" \
    --pod "$POD" \
    --detach \
    --env "WEBPASSWORD=$PIHOLE_PASSWORD" \
    docker.io/pihole/pihole:latest

# Grafana
podman run \
    --name "${POD}_grafana" \
    --pod "$POD" \
    --detach \
    docker.io/grafana/grafana-oss:latest

# Grafana Web UI (InfluxDB v2)
# Query language: Flux
# URL: $INFLUXDB_URL
# Access: Server
# Basic Auth: no
# Organization: $INFLUXDB_ORG
# Token: $INFLUXDB_TOKEN
# Default bucket: default

# pihole2influxdb
podman run \
    --name py \
    --tty \
    --interactive \
    --rm \
    --volume "$PWD:/data" \
    --workdir /data \
    --security-opt label=disable \
    --network host \
    --env INFLUXDB_URL="$INFLUXDB_URL" \
    --env INFLUXDB_TOKEN="$INFLUXDB_TOKEN" \
    --env INFLUXDB_ORG="$INFLUXDB_ORG" \
    --env INFLUXDB_BUCKET="$INFLUXDB_BUCKET" \
    --env PIHOLE_URL="$PIHOLE_URL" \
    --env PIHOLE_PASSWORD="$PIHOLE_PASSWORD" \
    --env PIHOLE_INTERVAL="$PIHOLE_INTERVAL" \
    docker.io/python:alpine sh
