#!/usr/bin/env python
"""Send PiHole logs to InfluxDB"""

from datetime import datetime
from enum import Enum
from hashlib import sha256
from os import environ
from time import sleep, time
from urllib3.util import Retry
from influxdb_client import InfluxDBClient, Point  # type: ignore
from influxdb_client.client.write_api import SYNCHRONOUS  # type: ignore
from requests import Session
from requests.adapters import HTTPAdapter

__version__ = "1.2.11"
INFLUXDB_URL = environ["INFLUXDB_URL"]
INFLUXDB_TOKEN = environ["INFLUXDB_TOKEN"]
INFLUXDB_ORG = environ["INFLUXDB_ORG"]
INFLUXDB_BUCKET = environ["INFLUXDB_BUCKET"]
PIHOLE_URL = environ["PIHOLE_URL"]
PIHOLE_PASSWORD = environ["PIHOLE_PASSWORD"]
PIHOLE_INTERVAL = int(environ["PIHOLE_INTERVAL"])


class QueryStati(Enum):
    """
    https://docs.pi-hole.net/database/ftl/#supported-status-types
    """

    UNKNOWN = "0"  # Unknown status (not yet known)
    BLOCKED = "1"  # Domain contained in gravity database
    FORWARDED = "2"
    CACHED = "3"  # Known, replied to from cache
    BLACKLIST_REGEX = "4"
    BLACKLIST_EXACT = "5"
    BLOCKED_UPSTREAM = "6"
    BLOCKED_UPSTREAM_ZERO = "7"
    BLOCKED_UPSTREAM_NXDOMAIN = "8"
    BLOCKED_GRAVITY = "9"
    BLOCKED_GRAVITY_REGEX = "10"
    BLOCKED_GRAVITY_EXACT = "11"
    ALLOWED_RETRIED = "12"
    ALLOWED_RETRIED_IGNORED = "13"
    ALLOWED_FORWARDED = "14"
    BLOCKED_DATABASE_BUSY = "15"
    BLOCKED_SPECIAL_DOMAIN = "16"
    ALLOWED_STALE_CACHE = "17"


class ReplyTypes(Enum):
    """
    https://docs.pi-hole.net/database/ftl/#supported-reply-types
    """

    UNKNOWN = "0"  # no reply so far
    NODATA = "1"
    NXDOMAIN = "2"
    CNAME = "3"
    IP = "4"  # a valid IP record
    DOMAIN = "5"
    RRNAME = "6"
    SERVFAIL = "7"
    REFUSED = "8"
    NOTIMP = "9"
    OTHER = "10"
    DNSSEC = "11"
    NONE = "12"  # query was dropped intentionally
    BLOB = "13"  # binary data


def store(data: list[Point]) -> None:
    """Connect to InfluxDB"""

    client = InfluxDBClient(
        url=INFLUXDB_URL,
        token=INFLUXDB_TOKEN,
        org=INFLUXDB_ORG,
    )

    # Check bucket exist
    buckets_api = client.buckets_api()
    buckets = buckets_api.find_buckets().buckets
    if not any(b for b in buckets if b.name == INFLUXDB_BUCKET):
        _ = client.buckets_api().create_bucket(
            bucket_name=INFLUXDB_BUCKET,
            org=INFLUXDB_ORG,
        )

    # Write records
    with client.write_api(write_options=SYNCHRONOUS) as api:
        api.write(
            bucket=INFLUXDB_BUCKET,
            org=client.org,
            record=data,
        )


def get_data(url: str) -> Point:
    """Request url and return json"""

    adapter = HTTPAdapter(
        max_retries=Retry(
            total=5,
            backoff_factor=1,  # 0.5s, 1s, 2s, 4s, 8s
            status_forcelist=[429, 500, 502, 503, 504],
            allowed_methods=["HEAD", "GET", "OPTIONS"],
        ),
    )
    http = Session()
    http.mount("http://", adapter)  # nosemgrep
    http.mount("https://", adapter)

    return http.get(url).json()


def get_logs(url: str) -> Point:
    """Get Pi-Hole logs"""

    rows = get_data(url=url)["data"]
    print(f"{datetime.now()} | Read {len(rows)}")
    for row in rows:
        (
            timestamp,
            query_type,
            domain,
            client,
            status,
            destination,
            reply_type,
            reply_time,
            _,
            _,
            upstream,
            _,
        ) = row
        point = (
            Point("logs")
            .time(datetime.fromtimestamp(int(timestamp)))
            .tag("query_type", query_type)
            .tag("domain", domain)
            .tag("client", client)
            .tag("status", QueryStati(status).name)
            .tag("destination", destination)
            .tag("reply_type", ReplyTypes(reply_type).name)
            .field("reply_time_ms", int(reply_time))
            .tag("upstream", upstream)
        )
        yield point


def get_summary(url: str) -> Point:
    """Get Pi-Hole summary"""

    status_dct = {
        "enabled": 1,
        "disabled": 0,
    }

    data = get_data(url=url)
    print(f"{datetime.now()} | Read summary")
    point = (
        Point("summary")
        .time(datetime.now())
        .field("domains_being_blocked", data["domains_being_blocked"])
        .field("gravity_last_updated", int(data["gravity_last_updated"]["absolute"]))
        .field("status", status_dct[data["status"]])
    )

    yield point


if __name__ == "__main__":
    # PiHole's web token is just a double sha256 hash of the utf8 encoded password
    PIHOLE_TOKEN = sha256(sha256(PIHOLE_PASSWORD.encode()).hexdigest().encode()).hexdigest()
    PIHOLE_LOGS = f"{PIHOLE_URL}/admin/api.php?getAllQueries&auth={PIHOLE_TOKEN}"
    APIS = {
        "logs": [
            get_logs,
            PIHOLE_LOGS,
        ],
        "summary": [
            get_summary,
            f"{PIHOLE_URL}/admin/api.php?summaryRaw&auth={PIHOLE_TOKEN}",
        ],
    }

    # Store all data until now
    print(f"Gather all data from {PIHOLE_URL}")
    for fn, endpoint in APIS.values():
        store(fn(endpoint))  # type: ignore

    while True:
        sleep(PIHOLE_INTERVAL)
        now = int(time())
        api_from = now - PIHOLE_INTERVAL - 10
        api_until = now + 10
        APIS["logs"][1] = f"{PIHOLE_LOGS}&from={api_from}&until={api_until}"
        for fn, endpoint in APIS.values():
            store(fn(endpoint))  # type: ignore
