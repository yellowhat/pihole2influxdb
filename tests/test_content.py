#!/usr/bin/env python
"""Send PiHole logs to InfluxDB"""

from os import environ
from pytest import fixture
from influxdb_client import InfluxDBClient  # type: ignore

INFLUXDB_URL = environ["INFLUXDB_URL"]
INFLUXDB_TOKEN = environ["INFLUXDB_TOKEN"]
INFLUXDB_ORG = environ["INFLUXDB_ORG"]
INFLUXDB_BUCKET = environ["INFLUXDB_BUCKET"]


@fixture(name="client")
def setup_client() -> InfluxDBClient:
    """InfluxDB client"""

    return InfluxDBClient(
        url=INFLUXDB_URL,
        token=INFLUXDB_TOKEN,
        org=INFLUXDB_ORG,
    )


def test_bucket(client: InfluxDBClient) -> None:
    """Check bucket exist"""

    buckets_api = client.buckets_api()
    buckets = buckets_api.find_buckets().buckets
    assert any(b for b in buckets if b.name == INFLUXDB_BUCKET)


def test_logs(client: InfluxDBClient) -> None:
    """Check logs"""

    result = client.query_api().query(
        org=INFLUXDB_ORG,
        query=f"""
            from(bucket: "{INFLUXDB_BUCKET}")
              |> range(start: -1y)
              |> filter(fn:(r) => r._measurement == "logs")
        """,
    )

    values = [record.values for table in result for record in table.records]
    assert len(values) >= 3

    domains = [table["domain"] for table in values]
    assert "google.com" in domains


def test_summary(client: InfluxDBClient) -> None:
    """Check summary"""

    fields_ref = {
        "domains_being_blocked",
        "gravity_last_updated",
        "status",
    }

    result = client.query_api().query(
        org=INFLUXDB_ORG,
        query=f"""
            from(bucket: "{INFLUXDB_BUCKET}")
              |> range(start: -1y)
              |> filter(fn:(r) => r._measurement == "summary")
        """,
    )

    values = [record.values for table in result for record in table.records]
    assert len(values) >= 3

    fields = {res["_field"] for res in values}
    assert fields_ref == fields
