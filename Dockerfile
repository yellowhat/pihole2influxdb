FROM python:3.11.3-alpine3.17@sha256:2659ee0e84fab5bd62a4d9cbe5b6750285e79d6d6ec00d8e128352dad956f096

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

COPY requirements.txt /requirements.txt

RUN pip install --no-cache-dir --requirement /requirements.txt

WORKDIR /app

COPY pihole2influxdb.py /app/

USER guest

CMD ["python3", "-u", "pihole2influxdb.py"]
