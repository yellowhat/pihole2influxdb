# Semantic release

The configuration file for [Semantic release](https://github.com/semantic-release/semantic-release)
is `.releaserc.yaml`.

## Requirements

### GitLab Token

Required to interact with GitLab: create tags, create `CHANGELOG.md`.

Login to `gitlab.com` → top right corner → `Edit Profile` → `Access Token`:

* `Token name`: `SEMANTIC_RELEASE`
* `Expiration date`
* Select scopes:
  * `api`
  * `write_repository`

### CI Variables

From the GitLab repository → `Settings` → `CI/CD` → `Variables` → `Add variable`:

1. GitLab
    * `Key`: `GITLAB_TOKEN`
    * `Value`: GitLab token above
    * [ ] Protect variable
    * [x] Mask variable
