**Archived as not using Pi-Hole anymore**

# pihole2influxdb

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD013 -->

 [![pipeline status](https://gitlab.com/yellowhat-labs/pihole2influxdb/badges/main/pipeline.svg)](https://gitlab.com/yellowhat-labs/pihole2influxdb/-/commits/main)
 [![Latest Release](https://gitlab.com/yellowhat-labs/pihole2influxdb/-/badges/release.svg)](https://gitlab.com/yellowhat-labs/pihole2influxdb/-/releases)

<!-- markdownlint-enable MD013 -->
<!-- markdownlint-restore -->

Leverage Pi-Hole API to gather data about a Pi-Hole instance
and store it in InfluxDB.

## Requirements

* [InflxDB](https://www.influxdata.com)
* [PiHole](https://pi-hole.net)

Environment variables:

| Key               | Value                      |
|-------------------|----------------------------|
| `INFLUXDB_URL`    | InfluxDB endpoint url      |
| `INFLUXDB_TOKEN`  | InfluxDB token             |
| `INFLUXDB_ORG`    | InfluxDB organization      |
| `INFLUXDB_BUCKET` | InfluxDB bucket to be used |
| `PIHOLE_URL`      | Pi-Hole endpoint url       |
| `PIHOLE_PASSWORD` | Pi-Hole admin password     |
| `PIHOLE_INTERVAL` | Interval in seconds        |
