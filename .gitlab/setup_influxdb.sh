#!/usr/bin/env bash
# Setup InfluxDB
set -xeuo pipefail

curl -L https://dl.influxdata.com/influxdb/releases/influxdb2-client-2.5.0-linux-amd64.tar.gz |
    tar xz --strip-components 1

./influx setup \
    --host "$INFLUXDB_URL" \
    --username user \
    --password password \
    --token "$INFLUXDB_TOKEN" \
    --org "$INFLUXDB_ORG" \
    --bucket default \
    --retention 0 \
    --force

./influx user list
